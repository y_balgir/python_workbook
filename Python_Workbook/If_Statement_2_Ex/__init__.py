def Exercice_34():
    num = input("Enter any integer number ->")
    
    if num % 2 == 0:
        print("Its an even number")
    else:
        print("Its an odd number")
    return


def Exercice_35():
    num = input("Please enter no. of human years -> ")    

    if num < 0:
        print("Negative human years not allowed")
        return
    elif num > 0 and num <= 2:
        dogyears = num * 10.5
    else:
        dogyears = 2 * 10.5 + (num - 2) * 4
    
    print("Equivalent dog years -> %.1f" % (dogyears))
    return

def Exercice_36():

    char = raw_input("Please enter ana alphabet -> ")
    vowels = set(['a','e','i','o','u',])

    if char in vowels:
        print("Alphabet enetered is a vowel.")       
    elif char == 'y':
        print("Alphabet y is sometimes a vowel and sometimes a consonent.")
    else:
        print("Alphabet enetered is a consonent.")
    
    return


def Exercice_37():
    num = input("Enter no. of sides of the polygon->")
    
    if num == 3:
        print("It's a Triangle.")
    elif num == 4:
        print("It's a Quadrilateral or Tetragon.")
    elif num == 5:
        print("It's a Pentagon.")
    elif num == 6:
        print("It's a Hexagon.")
    elif num == 7:
        print("It's a Heptagon.")
    elif num == 8:
        print("It's a Octagon.")
    elif num == 9:
        print("It's a Enneagon.")
    elif num == 10:
        print("It's a Decagon.")
    else:
        print("Its not a supported polygon.")
    return

    return


def Exercice_38():
    
    month = raw_input("Enter name of the month -> ")
    
    if month == 'jan' or month == 'mar' or month == 'may' or month == 'jul' or \
        month == 'aug' or month == 'oct' or month == 'dec' :
        print("This month contain 31 days.")
    elif month == 'feb':
        print("This month contain 28 or 29 days.")
    else:
        print("This month contain 30 days.")

    return


DB_Quiet_room = 40
DB_Alarm_clock = 70
DB_Gas_lawnmower = 106
DB_Jackhammer = 130

def Exercice_39():    
    dbLevel = int(input("Enter the noise level in Db -> "))
    
    exact = ""
    upper = ""
    lower = ""

    if dbLevel == DB_Quiet_room:
        exact = "Quiet room"
    elif dbLevel > DB_Quiet_room and dbLevel < DB_Alarm_clock:
        lower = "Quiet room"
        upper = "Alarm clock"        
    elif dbLevel == DB_Alarm_clock:
        exact = "Alarm clock"
    elif dbLevel > DB_Alarm_clock and dbLevel < DB_Gas_lawnmower:
        lower = "Alarm clock"
        upper = "Gas lawnmower"
    elif dbLevel == DB_Gas_lawnmower:
        exact = "Gas lawnmower"
    elif dbLevel > DB_Gas_lawnmower and dbLevel < DB_Jackhammer:
        lower = "Gas lawnmower"
        upper = "Jackhammer"
    elif dbLevel == DB_Jackhammer:
        exact = "Jackhammer"

    if exact:
        print "Db level is exactly that of a " + exact
    elif (lower) and (upper):
        print "Db level is between " + lower + " and " + upper
    else:
        print "Unreckognised Db level !!"

    return

def Exercice_40():
    a = int(input("Please enter the first side of the triange -> "))
    b = int(input("Please enter the second side of the triange -> "))
    c = int(input("Please enter the third side of the triange -> "))

    #if a == b == c:
    #    print("Enterd triangle is an equilateral triangle.")
    #elif a==b or a==c or b==c:
    #    print("Enterd triangle is an isocelese triangle.")
    #else:
    #    print("Enterd triangle is an scalene triangle.")

    if a == b == c:
        name = "equilateral triangle."
    elif a == b or a == c or b == c:
        name = "isocelese triangle."
    else:
        name = "scalene triangle."
    
    print "Triangle entered is a " + name     

    return




def Exercice_41():
    
    noteName = raw_input("Please enter the name of the note -> ")

    
    print noteName[0]
    
    print noteName[1]

    return

def Exercice_42():
    return

def Exercice_43():
    return
