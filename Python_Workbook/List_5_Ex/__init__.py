#region Exercice_104
def Exercice_104():
    
    listOfNumbers = []
    condition = True
    while condition:
        incomingNo = int(input("Please enter a number -> "))
        
        if incomingNo == 0:
            break
        
        listOfNumbers.append(incomingNo)
    
    

    print listOfNumbers
    print sorted(listOfNumbers)
    
    list.sort(listOfNumbers)
    print listOfNumbers

    return
#endregion Exercice_104

#region Exercice_105
def Exercice_105():

    listOfNumbers = []
    condition = True
    while condition:
        incomingNo = int(input("Please enter a number -> "))
        
        if incomingNo == 0:
            break
        
        listOfNumbers.append(incomingNo)
    
    

    print listOfNumbers
    print sorted(listOfNumbers, reverse=True)
    
    list.sort(listOfNumbers, reverse=True)
    print listOfNumbers

    return
#endregion Exercice_105

#region Exercice_106
def Exercice_106():

    listOfNumbers = []
    condition = True
    while condition:
        incomingNo = int(input("Please enter a number -> "))
        
        if incomingNo == 0:
            break
        
        listOfNumbers.append(incomingNo)

    countToRem = int(input("\nPlease enter a number of outliers to remove -> "))
    
    #finalOutcome = OutlierRemover_V1(listOfNumbers,countToRem)
    #finalOutcome = OutlierRemover_V2(listOfNumbers,countToRem)
    finalOutcome = OutlierRemover_V3(listOfNumbers,countToRem)

    print "\nIncoming list -> \n",listOfNumbers
    print "\nFinal outcome -> \n",finalOutcome    
    return

def OutlierRemover_Experiment(incomingList, count):

    listToBeRetruned = sorted(incomingList)
    #listToBeRetruned = listToBeRetruned[count * (-1):]
    print "\nIncoming list -> \n",listToBeRetruned

    Alpha = listToBeRetruned[count :]
    print "\n listToBeRetruned[count :] \n",Alpha
    
    Alpha = listToBeRetruned[:count]
    print "\n listToBeRetruned[:count] \n",Alpha

    Alpha = listToBeRetruned[count * (-1):]
    print "\n listToBeRetruned[count * (-1):] \n",Alpha

    Alpha = listToBeRetruned[:count * (-1)]
    print "\n listToBeRetruned[:count * (-1)] \n",Alpha
    
    return listToBeRetruned

def OutlierRemover_V1(incomingList, count):

    listToBeRetruned = sorted(incomingList)
    listToBeRetruned = listToBeRetruned[count:]
    listToBeRetruned = listToBeRetruned[:count * (-1)]
    
    return listToBeRetruned

def OutlierRemover_V2(incomingList, count):

    listToBeRetruned = sorted(incomingList)
    
    for i in range(count):
        listToBeRetruned.pop()
        listToBeRetruned.pop(0)
    
    return listToBeRetruned

def OutlierRemover_V3(incomingList, count):

    listToBeRetruned = sorted(incomingList)
    i = 0
    while i < count:
        listToBeRetruned.pop()
        listToBeRetruned.pop(0)
        i+=1
    
    return listToBeRetruned

#endregion Exercice_106

#region Exercice_107
def Exercice_107():

    listOfWords = []
    condition = True
    while condition:
        incomingWord = raw_input("Please enter a word -> ")
        
        if incomingWord == '':
            break
        
        listOfWords.append(incomingWord)
    
    #alpha = set(listOfWords)

    print listOfWords
    print DistinctWord_V1(listOfWords)

    return

def DistinctWord_V1(incomingIterable):
    
    returnIterable = []
    
    for word in incomingIterable:
        if word not in returnIterable:
            returnIterable.append(word)

    return returnIterable
#endregion Exercice_107

#region Exercice_108
def Exercice_108():
    
    listOfNumbers = []
    condition = True
    while condition:
        incomingNo = raw_input("Please enter a number -> ")
        
        if incomingNo == '':
            break
        
        listOfNumbers.append(int(incomingNo))    
    

    print "Original List -> \n",listOfNumbers
    Alpha = NumberSeperator(listOfNumbers)
    print "New List -> \n",Alpha

    return

def NumberSeperator(incomingIterable):
    negatives = []
    zeros = []    
    positives = []

    for number in incomingIterable:
        if number < 0:
            negatives.append(number)
        elif number == 0:
            zeros.append(number)
        else:
            positives.append(number)

    return negatives + zeros + positives

#endregion Exercice_108

#region Exercice_109
def Exercice_109():

    inputNo = int(input("Please enter a no. for finding proper divisor -> "))
    
    print ProperDivisors(inputNo)
    
    return

def ProperDivisors(inputNo):
    returnValue = []
    i = 1    
    while i <= inputNo / 2:
        if inputNo % i == 0:
            returnValue.append(i)
        i+=1
    
    return returnValue
#endregion Exercice_109
def Exercice_110():
    
    perfectNumbers = []
    print "List of perfect numbers from 1 to 1000 -> "
    
    for num in range(1,1000,1):
        divisors = ProperDivisors(num)
        if sum(divisors) == num:
            perfectNumbers.append(num)

    print perfectNumbers
    return


def Exercice_111():
    individualWords = []
    
    completeSentence = raw_input("Please enter a sentence -> \n")

    individualWords = list(completeSentence.split())

    if '?' in individualWords:
        individualWords.remove('?')

    if ',' in individualWords:
        individualWords.remove(',')
    
    if '.' in individualWords:
        individualWords.remove('.')

    if '-' in individualWords:
        individualWords.remove('-')

    if '\'' in individualWords:
        individualWords.remove('\'')

    if '!' in individualWords:
        individualWords.remove('!')

    if ':' in individualWords:
        individualWords.remove(':')

    print individualWords
    return



#import random in Rand import
from random import randint as RandomIntegerRange

def Exercice_114():

    condition = True
    winningNumber = []

    while condition:
        newNo = RandomIntegerRange(1,49)
        if newNo not in winningNumber:
            winningNumber.append(newNo)
    
        condition = len(winningNumber) < 6

    print winningNumber

    return


def Exercice_117():

    condition = True
    CoordinateList = []
    tempList = []

    while condition:
        x = raw_input("\nPlease eneter X Coordinate - > ")
        y = raw_input("Please eneter Y Coordinate - > ")

        if x == '' or y == '':
            break        
        
        tempList.append(float(x))
        tempList.append(float(y))
        CoordinateList.append(tempList)
        tempList = list()
        
        #tempList = []
        #del tempList[:]

    print CoordinateList

    return


