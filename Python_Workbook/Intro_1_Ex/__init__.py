
def Exercise_1():
    print("""\t First Line of Address : 33B, Harlington Road East
    \t Second Line of Address : Feltham, Middlesex
    \t Third Line of Address : TW13 5BW
    """)
    return

def Exercise_2():
    readValue = raw_input("Please enter your name ")    
    print("Hello " + readValue)
    return

def Exercise_3():
    readWidth = raw_input("Please enter width of the room in feet: ")
    readLength = raw_input("Please enter length of the room feet: ")    
    area = float(readWidth) * float(readLength)
    ##print("Area of the room is " + str(area) + " Square foot")
    print "Area of the room is ", area ," Square foot"
    return


SQUARE_FEET_TO_ACRE_CONVERTER_UNIT = 43560
def Exercise_4():
    readLength = float(raw_input("Please enter length of field : "))
    readWidth = float(raw_input("Please enter width of field : "))
    ##print("Area in acres is ","asdsad","sad")
    ##print "Area in acres is " , ((readLength * readWidth) / 43560) , " Acres"
    print "Area in acres is " , ((readLength * readWidth) / SQUARE_FEET_TO_ACRE_CONVERTER_UNIT) , " Acres"
    return

REFUND_SUB_ONE_LITRE_BOTTLE = 0.1
REFUND_OVER_ONE_LITRE_BOTTLE = 0.25
def Exercise_5():
    oneLitreBottles = round(float(input("Enter No. of bottles with capacity one litre and below : ")))
    aboveOneLitreBottles = round(float(input("Enter No. of bottles with capacity above one litre : ")))
    amountToRefund = round((oneLitreBottles * REFUND_SUB_ONE_LITRE_BOTTLE) + (aboveOneLitreBottles * REFUND_OVER_ONE_LITRE_BOTTLE),2)
    ##amountToRefund = (oneLitreBottles * 0.1) + (aboveOneLitreBottles *
    ##0.6565655)
    print "You are eligible for a refund of ", amountToRefund, " $"
    return

TAX_RATE = .12
TIP_RATE = .18
def Exercise_6():
    mealCost = input("Please enter the cost of your meal : ")
    taxAmount = TAX_RATE * .12
    tipAmount = TIP_RATE * .18
    ##print("Meal amount : %.2f" % mealCost)
    print("\n Meal amount : %f" % mealCost)
    print(" Tax amount : %f" % taxAmount)
    print(" Tip amount : %f" % tipAmount)    
    print("\n Meal cost: %f || Tax amount: %f || Tip amount: %f" % (mealCost,taxAmount, tipAmount))
    return

def Exercise_7():
    inputNumber = int(input("Enter the number for addition : "))
    sum = (inputNumber * (inputNumber + 1)) / 2
    print("Final result is : %d" % (sum))
    return

WIDGET_WEIGHT = 75
GIZMO_WEIGHT = 112
def Exercise_8():
    inputWidgets = int(input("Enter the number of widgets : "))
    inputGizmos = int(input("Enter the number of Gizmos  : "))
    weightInGrams = inputWidgets * WIDGET_WEIGHT + inputGizmos * GIZMO_WEIGHT
    print("Weight in grams : %f" % weightInGrams)
    ##print("Weight in Kgs : %.2f" % (weightInGrams/1000))
    print("Weight in Kgs   : %f" % round(weightInGrams / 1000,8))
    return

RATE_OF_INTEREST = .05
def Exercise_9():
    inputAmount = float(input("Enter the amount deposited in dollars : "))
    print("Interest earned at the end of year 1 -> %.2f ,Final amount at the end of year 1 -> %.2f" % (inputAmount * RATE_OF_INTEREST,inputAmount * (1 + RATE_OF_INTEREST)))
    print("Interest earned at the end of year 2 -> %.2f ,Final amount at the end of year 2 -> %.2f" % \
                ((inputAmount * RATE_OF_INTEREST) ** 2, inputAmount * ((1 + RATE_OF_INTEREST) ** 2)))
    print("Interest earned at the end of year 3 -> %.2f ,Final amount at the end of year 3 -> %.2f" % \
                ((inputAmount * RATE_OF_INTEREST) ** 3, inputAmount * ((1 + RATE_OF_INTEREST) ** 3)))
    return


from math import log10
def Exercise_10():
    numberA = float(input("Please enter number A : "))
    numberB = float(input("Please enter number B : "))
    print(" A + B -> %0.2f" % (numberA + numberB))
    print(" A - B -> %0.2f" % (numberA - numberB))
    print(" A * B -> %0.2f" % (numberA * numberB))
    print(" A / B -> %0.2f" % (numberA / numberB))
    print(" A / B -> %d" % (round(numberA / numberB)))
    print(" A remainder B -> %0.2f" % (numberA % numberB))
    print(" Log10(A) -> %0.2f" % (log10(numberA)))
    print(" A ^ B -> %0.2f" % (numberA ** numberB))
    asdsad = (int(numberA / numberB))
    sdfsdf = numberA // numberB
    return

def Exercise_11():
    mpgInput = input("Please enter the MPG number -> ")    
    print("%.2f MPG in Liter per 100km is %.2f" % (mpgInput,235.214 / mpgInput))
    return

from math import (radians,acos,sin,cos)
RADIUS_OF_EARTH = 6371.01
def Exercise_12():
    Lat_1 = input("Kindly enter the Lattitude of point 1 in degrees - > ")
    Lon_1 = input("Kindly enter the Longitude of point 1 in degrees - > ")
    print("")
    Lat_2 = input("Kindly enter the Lattitude of point 2 in degrees - > ")
    Lon_2 = input("Kindly enter the Longitude of point 2 in degrees - > ")

    Lat_1 = radians(Lat_1)
    Lat_2 = radians(Lat_2)
    
    Lon_1 = radians(Lon_1)
    Lon_2 = radians(Lon_2)

    DistanceBetweenThePoints = RADIUS_OF_EARTH * acos(sin(Lat_1) * sin(Lat_2) + cos(Lat_1) * cos(Lat_2) * cos(Lon_1 - Lon_2))

    #print "Distance between the two points is ->", DistanceBetweenThePoints
    print ("Distance between the two points is -> %0.3f" % DistanceBetweenThePoints)
    return


TOONIES = 200
LOONIES = 100
QUARTERS = 25
DIMES = 10
NICKELS = 5
PENNIES = 1
def Exercise_13():
    AmountInCents = input("Enter the number of cents to be converted -> ")
    
    toonies = AmountInCents // TOONIES
    #AmountInCents -= toonies * TOONIES
    AmountInCents %= TOONIES  

    loonies = AmountInCents // LOONIES
    #AmountInCents -= loonies * LOONIES
    AmountInCents %=  LOONIES

    quarters = AmountInCents // QUARTERS
    #AmountInCents -= quarters * QUARTERS
    AmountInCents %= QUARTERS

    dimes = AmountInCents // DIMES
    #AmountInCents -= quarters * DIMES
    AmountInCents %= DIMES

    nickels = AmountInCents // NICKELS
    #AmountInCents -= quarters * DIMES
    AmountInCents %= NICKELS

    pennies = AmountInCents // PENNIES
    #AmountInCents -= pennies * PENNIES
    AmountInCents %= PENNIES

    print("""\n Amount to be dispensed  \n
    \t Toonies  -> %d
    \t Loonies  -> %d
    \t Quarters -> %d
    \t Dimes    -> %d
    \t Nickels  -> %d
    \t Pennies  -> %d
    """ % (toonies,loonies,quarters,dimes,nickels,pennies))
    return

def Exercise_14():
    feet = input("Enter the feet part of height -> ")
    inch = input("Enter the inches part of height -> ")
    print("Your height in cm -> %0.2f" % ((feet * 12 + inch) * 2.54))
    return

def Exercise_15():    
    feet = input("Enter the feet to be converted -> ")
    print("\n\t Entered feet in inches -> %0.2f" % (feet * 12))
    print("\n\t Entered feet in yards -> %0.2f" % (feet * (1 / 3)))
    print("\n\t Entered feet in miles -> %0.2f" % (feet * 0.000189394))
    return


from math import pi as PIE
def Exercise_16():
    radius = input("Enter the radius -> ")
    print("\n\t Area of circle -> %0.2f" % (PIE * (radius ** 2)))
    print("\n\t Volume of sphere -> %0.2f \n" % ((4 / 3) * PIE * (radius ** 3)))    
    return

HEAT_CAPACITY_OF_WATER = 4.186
JOULES_TO_KWH = 2.777778e-7
COST_KWH = 8.9
def Exercise_17():
    mass = input("Enter mass of water to be considered -> ")
    t1 = input("Enter initial temperature -> ")
    t2 = input("Enter final temperature -> ")
    
    energyInJoules = mass * HEAT_CAPACITY_OF_WATER * (t2 - t1)
    energyInKWH = energyInJoules * JOULES_TO_KWH
    
    print("Energy required in joules  -> %0.2f" % (energyInJoules))
    print("Energy required in KWH  -> %0.2f" % (energyInKWH))
    print("Cost of energy -> %0.2f" % (energyInKWH * COST_KWH))
    return

def Exercise_18():
    radius = input("Enter radius of cylinder -> ")
    height = input("Enter height of cylinder -> ")

    print("Volume of cylinder-> %0.1f" % ((radius ** 2) * PIE * height))
    return

from math import sqrt
GRAVITATIONAL_ACCELERATION = 9.8
def Exercise_19():
    height = input("Enter height of the drop -> ")    

    print("Speed of hitting the ground -> %0.1f" % (sqrt(2 * GRAVITATIONAL_ACCELERATION * height)))
    return

def Exercise_20():
    return

def Exercise_21():
    height = input("Enter height of tringle -> ")
    base = input("Enter base of tringle -> ")
    
    print("Area of triangle -> %0.1f" % (0.5 * base * height))
    return

def Exercise_22():
    s1 = input("Enter side 1 of tringle -> ")
    s2 = input("Enter side 2 of tringle -> ")
    s3 = input("Enter side 3 of tringle -> ")

    s = (s1 + s2 + s3) / 2

    print("Area of triangle -> %0.3f" % (sqrt(s * (s - s1) * (s - s2) * (s - s3))))
    return

from math import tan
def Exercise_23():
    n = input("Enter number of sides of the polygon -> ")
    s = input("Enter length of each side of polygon -> ")
    
    area = (s ** 2 * n) / (4 * tan(PIE / n))

    print("Area of polygon -> %0.3f" % (area))
    return

SECs_IN_DAY = 86400
SECs_IN_HOUR = 3600
SECs_IN_MIN = 60
def Exercise_24():
    d = input("Enter number of days -> ")
    h = input("Enter number of hours -> ")
    m = input("Enter number of minutes -> ")
    s = input("Enter number of seconds -> ")
    
    totalSeconds = d * SECs_IN_DAY + h * SECs_IN_HOUR + m * SECs_IN_MIN + s

    print("Total seconds -> %d" % (totalSeconds))
    return

def Exercise_25():
    sec = input("Enter total number of seconds -> ")
    
    return

def Exercise_26():
    return

def Exercise_27():
    return

