def Exercice_61():
    sum = 0
    count = 0
    condition = True

    while condition:
        incomingNo = int(input("Please enter a number -> "))
        
        if incomingNo == 0:
            break

        count+=1
        sum+=incomingNo

    if  count > 0:          
        print("Average is -> %0.2f" % (sum / count))   

    return

def Exercice_62():
    
    amounts = [4.95, 9.95, 14.95, 19.95 , 24.95]
    
    for i in amounts:
        print ("Current price -> %0.2f" % (i))
        print ("Discounted amount -> %0.2f" % (i * (0.6)))
        print ("Current price -> %0.2f \n" % (i * (0.4)))
    
    return


DEGREE_SYMBOL = u'\N{DEGREE SIGN}'
def Exercice_63():
    temperatureRange = range(0,100,10)
    
    for currentTemp in temperatureRange:
        print(" " + DEGREE_SYMBOL + "C " + "-> %d" % (currentTemp) + " into " \
                + DEGREE_SYMBOL + "F " + "-> %0.2f" % ((currentTemp * 1.8) + 32))
    return

def Exercice_64():
    
    return

def Exercice_65():
    return

def Exercice_66():
    return

def Exercice_67():
    return

def Exercice_68():
    return

def Exercice_69():
    return

def Exercice_70():
    return


